package net.tncy.fva.myproject1;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTest 
{
    @Test
    public void divisionTest()
    {
        Calculator calculatrice = new Calculator();
        assertEquals(3.0,calculatrice.division(6, 2),0);
    }
}
